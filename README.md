HandsOnTable

http://handsontable.com/

is a great javascript library that can be used in many different ways.
We have prepared a plugin for web2py that allows to use it for the following purpose:
You have two tables t1 and t2 (example: db.auth_user and db.item) and a table
t3 that is a "product" of t1 and t2 (this user wants this much of this item).

A natural syntax gives you a grid whose rows are entries of t1, whose columns are
entries of t2, and which allows you to edit a number at the grid 
that crosses row and column.

[More info on its usage](https://gitlab.com/the_pang/plugin_hotsheet/wikis/home)